/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   connection_functions.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmors-ma <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/10 00:29:57 by tmors-ma          #+#    #+#             */
/*   Updated: 2019/02/10 00:29:58 by tmors-ma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "xalg.h"

void	disconnect_secondary_columns(t_x *root)
{
	t_x *t;

	t = root->r;
	while (t != root)
	{
		t = t->r;
		if (!t->l->p)
		{
			t->l->r = t->l;
			t->l->l = t->l;
		}
	}
	t = t->r;
	while (t->r->p)
		t = t->r;
	t->r = root;
	root->l = t;
}

/*
** reconnect_secondary_columns: for clearing (reconnects in a traverse order)
*/

void	reconnect_secondary_columns(t_x *root)
{
	t_x *co;

	co = root;
	while ((co = co->dr) != root)
	{
		co->r = co->dr;
		co->l = co->dl;
	}
	co->r = co->dr;
	co->l = co->dl;
}
