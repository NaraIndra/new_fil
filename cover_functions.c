/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cover_functions.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmors-ma <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/10 00:29:36 by tmors-ma          #+#    #+#             */
/*   Updated: 2019/02/10 00:29:37 by tmors-ma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "xalg.h"

void	cover_column(t_x *c)
{
	t_x *i;
	t_x *j;

	c->r->l = c->l;
	c->l->r = c->r;
	i = c->d;
	while (i != c)
	{
		j = i->r;
		while (j != i)
		{
			j->d->u = j->u;
			j->u->d = j->d;
			j->c->s -= 1;
			j = j->r;
		}
		i = i->d;
	}
}

void	uncover_column(t_x *c)
{
	t_x *i;
	t_x *j;

	i = c->u;
	while (i != c)
	{
		j = i->l;
		while (j != i)
		{
			j->c->s += 1;
			j->d->u = j;
			j->u->d = j;
			j = j->l;
		}
		i = i->u;
	}
	c->r->l = c;
	c->l->r = c;
}

void	cover_inner_columns(t_x *r)
{
	t_x *j;

	j = r->r;
	while (j != r)
	{
		cover_column(j->c);
		j = j->r;
	}
}

void	uncover_inner_columns(t_x *r)
{
	t_x *j;

	j = r->l;
	while (j != r)
	{
		uncover_column(j->c);
		j = j->l;
	}
}
