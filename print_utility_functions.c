/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_utility_functions.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmors-ma <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/10 22:50:58 by tmors-ma          #+#    #+#             */
/*   Updated: 2019/02/10 22:50:58 by tmors-ma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "xalg.h"

void	print_row(t_x *row)
{
	t_x *x;

	x = row;
	ft_putchar('(');
	ft_putstr(x->c->n);
	ft_putstr("), ");
	while ((x = x->r) != row)
	{
		ft_putchar('(');
		ft_putstr(x->c->n);
		ft_putstr("), ");
	}
	ft_putchar('\n');
}

void	print_column_objects(t_x *root)
{
	t_x *t;

	t = root;
	ft_putstr("h(0)");
	while ((t = t->r) != root)
	{
		if (t->p)
			ft_putstr("[p]");
		ft_putstr(t->n);
		ft_putchar('(');
		ft_putstr(ft_itoa(t->s));
		ft_putstr("), ");
	}
	ft_putchar('\n');
}

void	print_structure(t_x *root)
{
	t_x		*t;
	t_x		*dn;
	t_x		*arr[10000];
	int		i;
	int		a;

	i = 0;
	a = 0;
	t = root;
	ft_bzero(arr, 10000);
	print_column_objects(root);
	while ((t = t->r) != root && t->p)
	{
		dn = t;
		while ((dn = dn->d) != t)
			print_structure_columns(&i, &a, dn, arr);
	}
}

void	print_structure_columns(int *i, int *a, t_x *dn, t_x *arr[])
{
	t_x		*e;
	int		a0;

	e = dn;
	a0 = *a;
	if (!in_arr(e, arr, i))
	{
		ft_putchar('(');
		ft_putstr(e->c->n);
		ft_putstr("), ");
		arr[(*a)++] = e;
		(*i)++;
	}
	while ((e = e->r) != dn)
		if (!in_arr(e, arr, i))
		{
			ft_putchar('(');
			ft_putstr(e->c->n);
			ft_putstr("), ");
			arr[(*a)++] = e;
			(*i)++;
		}
	if (a0 != *a)
		ft_putchar('\n');
}

int		print_solution_as_structure(t_list *solution)
{
	t_x		*r;
	t_list	*t;

	t = solution;
	ft_putendl("solution:");
	while (t)
	{
		r = (t_x*)t->content;
		ft_putchar('(');
		ft_putstr(r->c->n);
		ft_putstr("), ");
		while ((r = r->r) != (t_x*)t->content)
		{
			ft_putchar('(');
			ft_putstr(r->c->n);
			ft_putstr("), ");
		}
		ft_putchar('\n');
		t = t->next;
	}
	ft_putchar('\n');
	return (1);
}
