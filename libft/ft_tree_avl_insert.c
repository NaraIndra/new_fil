/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tree_avl_insert.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmors-ma <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/11 20:00:53 by tmors-ma          #+#    #+#             */
/*   Updated: 2019/01/11 20:00:53 by tmors-ma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_avl		*ft_tree_avl_insert(t_avl *root, t_avl *node)
{
	int blc;

	if (!root)
		return (node);
	if (root->key > node->key)
		root->left = AVL_INSERT(root->left, node);
	else if (root->key < node->key)
		root->right = AVL_INSERT(root->right, node);
	root->height = 1 + ft_max(
		AVL_GETHEIGHT(root->left), AVL_GETHEIGHT(root->right));
	if ((blc = AVL_GETBALANCE(root)) > 1 && node->key < root->left->key)
		return (AVL_ROTATER(root));
	if (blc < -1 && node->key > root->right->key)
		return (AVL_ROTATEL(root));
	if (blc > 1 && node->key > root->left->key)
	{
		root->left = AVL_ROTATEL(root->left);
		return (AVL_ROTATER(root));
	}
	if (blc < -1 && node->key < root->right->key)
	{
		root->right = AVL_ROTATER(root->right);
		return (AVL_ROTATEL(root));
	}
	return (root);
}
