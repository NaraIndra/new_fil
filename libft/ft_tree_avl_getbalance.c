/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tree_avl_getbalance.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmors-ma <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/13 19:53:06 by tmors-ma          #+#    #+#             */
/*   Updated: 2019/01/13 19:53:06 by tmors-ma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_tree_avl_getbalance(t_avl *node)
{
	if (!node)
		return (0);
	return (AVL_GETHEIGHT(node->left) - AVL_GETHEIGHT(node->right));
}
