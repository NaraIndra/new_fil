/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_figure_functions.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <mstygg@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/10 00:30:04 by tmors-ma          #+#    #+#             */
/*   Updated: 2019/02/25 00:00:52 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "xalg.h"
#define epsilon 1e-5

/*
** prepare_line: removes new lines, substitutes for ones and zeroes,
** shifts to the left, skips empty rows
*/

void	prepare_line(char *dst, char *src, int shift_margin, int map_size)
{
	int n;
	int x;
	int y;
	int cn;

	n = 0;
	x = 0;
	y = 0;
	cn = shift_margin;
	printf("shift_margin=%d\n", shift_margin);
	while (src[n] == '.' || src[n] == '\n')
		n++;
	while ((n -= 5) >= 0)
		src += 5;
	src += shift_margin;
	n = 0;
	printf("map_size=%d\n", map_size);
	while (*src)
	{
		if (n == 4)
		{
			printf("final dst=%s\n", dst);
			return ;
		}
		printf("ok\n");
		if (*src == '\n')
		{
			printf("src=n\n");
			printf("*dst=%s\n", dst);
			while(x%(map_size))
			{
				*(dst + x++%map_size + map_size * y) = '0';
				printf("x=%d, map_size=%d, dst=%s\n", x, map_size, dst);
				//getchar();
			}
			++y;
		}
		else if (*src == '#')
		{
			*(dst + x++%map_size + map_size * y) = '1';
			n++;
			printf("src=#, dst=%s\n", dst);
		}
		else if (*src == '.')
		{
			if (!cn)
			{
				*(dst + x++%map_size + map_size * y) = '0';
				printf("src=., dst=%s\n", dst);
				cn = shift_margin;
			}
			else 
				--cn;
		}

		src++;
		printf("*dst=%s!\n", dst);
		getchar();
	}
}

int		get_shift_margin(char *str)
{
	int i;
	int	new_line;
	int shift_margin;
	int length;

	length = 20;
	new_line = 1;
	shift_margin = 5;
	i = 0;
	while (length-- && shift_margin != 0)
	{
		if (*str == '.' && new_line)
			i++;
		else if (*str == '\n')
		{
			shift_margin = shift_margin > i ? i : shift_margin;
			new_line = 1;
			i = 0;
		}
		else if (new_line)
			new_line = 0;
		str++;
	}
	return (shift_margin);
}

/*
** get_figure: last read returns 0 and according to the task
** last tetramino must be 20 length, others are 21 (contain 2\n)
*/

int		get_figure(int fd, char *line, int prev_length, int map_size)
{
	int		ret;
	char	buf[22];
	printf("inside get_figure m_size=%d\n", map_size);
	getchar();
	ft_bzero(buf, 22);
	ft_bzero(line, map_size);
	ret = read(fd, buf, 21);
	if (!ret)
		return (prev_length == 21 || prev_length == 0 ? -1 : 0);
	if (!is_valid(buf))
		return (-1);
	prepare_line(line, buf, get_shift_margin(buf), map_size);
	return (ft_strlen(buf));
}

int		process_file(char *file_name, t_x *root, const char ***g_cols, int map_size)
{
	int		fd;
	char	figure[2];
	int		res;
	char	line[map_size * map_size];


	int i = 0;
	printf("(inside_proc)map_size=%d\n", map_size);
	while(*((*g_cols) + i))
	{
		printf("(proc_file)[%d]%s\n", i , *((*g_cols)+i));
		++i;
		//getchar();
	}
	printf("done!\n");
	fd = open(file_name, O_RDONLY);
	if (fd < 0)
		return (-1);
	res = 0;
	figure[0] = 'A';
	figure[1] = 0;
	while ((res = get_figure(fd, line, res, map_size)) > 0)
	{
		if (!create_matrix_row(root, figure, line, g_cols))
			return (-1);
		printf("lklk\n");
		figure[0]++;
	}
	return (1);
}