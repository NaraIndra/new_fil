/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_matrix_functions.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstygg <mstygg@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/10 00:30:11 by tmors-ma          #+#    #+#             */
/*   Updated: 2019/02/24 21:52:51 by mstygg           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "xalg.h"

//static char		*g_cols[17] = {"11", "12", "13", "14", "21", "22", "23",\
//	"24", "31", "32", "33", "34", "41", "42", "43", "44", 0};

t_x		*create_root(void)
{
	t_x *root;

	if (!(root = new_tx("h")))
		return (NULL);
	root->d = NULL;
	root->u = NULL;
	return (root);
}

int		create_column_objects(t_x *root, char ***g_cols)
{
	int		i;
	t_x		*new;

	i = 0;
	printf("inside_column_objects\n");
	while ((*g_cols)[i])
	{
		printf("inside_g_cols_i=%d, g_cols[%d]=%s\n", i, i, (*g_cols)[i]);
		if (!(new = new_tx((*g_cols)[i])))
		{
			clear_structure(&root);
			return (-1);
		}
		insert_left_tx(root, new);
		i++;
	}
	return (1);
}

t_x		*append_figure_type_column(t_x *root, char *figure)
{
	t_x *new;

	if (!(new = new_tx(figure)))
		return (NULL);
	while (root->r->p)
		root = root->r;
	insert_right_tx(root, new);
	return (root->r);
}

int		insert_figure(t_x *root, t_x *row, char *line, const char ***g_cols)
{
	int		i;
	int		p;
	t_x		*co;
	t_x		*new;

	p = 0;
	i = 0;
	while (p != 4 && *line)
	{
		printf("*line=%c\n", *line);
		if (*line == '1')
		{
			printf("s_p\n");
			printf("root=%p, \n", root);
			printf("*g_cols[i]=%s\n", (*g_cols)[i]);
			co = find_column_object_by_name(root, (*g_cols)[i]);
			printf("CO-N=%s",co->n);
			getchar();
			printf("f_obj\n");
			if (!(new = new_tx(0)))
				return (0);
			insert_up_tx(co, new);
			//printf("sss\n");
			insert_left_tx(row, new);
			p++;
		}
		printf("p=%d\n", p);
		line++;
		i++;
	}
	printf("aaa\n");
	return (1);
}

int		create_matrix_row(t_x *root, char *figure, char *line, const char ***g_cols)
{
	t_x *t;
	t_x *new;
	printf("matrix_row\n");
	int i = 0;
	while(*((*g_cols)+i))
	{
		printf("(inside_cr_matrix)[%d]%s, %p\n", i, *((*g_cols)+i), *((*g_cols)+i));
		++i;
		//g_cols++;
	}
	if (!(t = find_column_object_by_name(root, figure)))
	{
		printf("found_column_by_name\n");
		t = append_figure_type_column(root, figure);
	}
	if (!t || !(new = new_tx(0)))
		return (0);
	t = insert_up_tx(t, new);

	printf("in_row_3");
	return (insert_figure(root, t, line, g_cols));
}
